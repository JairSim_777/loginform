export interface ResponseI {
    //  status: ok - error. Depends on the answer
    status: string;
    // response is where all the data goes
    result: any; 
}