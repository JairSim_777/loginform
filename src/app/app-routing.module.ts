import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './screen/dashboard/dashboard.component';
import { EditComponent } from './screen/edit/edit.component';
import { LoginComponent } from './screen/login/login.component';
import { RegisterComponent } from './screen/register/register.component';




const routes: Routes = [
  {path:'', redirectTo:'login', pathMatch:'full' },
  {path:'login', component:LoginComponent},
  {path:'dashboard', component:DashboardComponent},
  {path:'register', component:RegisterComponent},
  {path:'edit', component:EditComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent, DashboardComponent, RegisterComponent, EditComponent]
