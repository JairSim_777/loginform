import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( private _api: ApiService) { }

  ngOnInit(): void {
// (num page)    
    this._api.getAllUsers(1).subscribe(data => {
      console.log(data);
    })
  }

}
