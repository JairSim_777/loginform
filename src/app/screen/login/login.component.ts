import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms'
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { LoginI } from '../../models/login.interface';
import { ResponseI } from '../../models/response.interface';
import { getMaxListeners } from 'process';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // array - ('valor', validacion )
  loginForm = new FormGroup({
    userName : new FormControl('', Validators.required),
    //correo: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),

  })

  //Inyecciones
  constructor(private api:ApiService, private router: Router ) { }

  // variable for errors
  errorStatus: boolean = false;
  errorMsj: any = "";


  ngOnInit(): void {
    this.checkLocalStorage();
  }

  checkLocalStorage(){
    if (localStorage.getItem('token')){
      this.router.navigate(['dashboard'])
    }
  }

  onLogin(form: LoginI) {
    console.log(form)
    this.api.loginByEmail(form).subscribe(data => {
      let dataResponse: ResponseI = data;
      // validation data, dataresponse = respuesta api 
      if(dataResponse.status == "ok"){
      // if( dataResponse == "true"){
        // token: storage variable name
        localStorage.setItem("token", dataResponse.result.token); 
        this.router.navigate(['dashboard']); 
      }else {
        this.errorStatus = true;
        // el texto que envia el api (El password es invalido),
        // el usuario jsvt@gmail.com no existe
        this.errorMsj = dataResponse.result.error_msg;
      }

      console.log(data);
    })

  }

}
