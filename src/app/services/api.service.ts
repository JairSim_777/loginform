// Si es de utilidad, también tiene swagger activado: http://3.15.92.223:5050/swagger/index.html

import { Injectable } from '@angular/core';
import { LoginI } from '../models/login.interface';
import { ResponseI } from '../models/response.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'; 

import { ListUsersI } from '../models/listUsers.interface';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  _url:string = "http://3.15.92.223:5050/api/"
  //express
  // _url:string = "http://localhost:3000/api/"

  // _url:string = "http://solodata.es/";

  constructor(private http: HttpClient) { }
  // authentication: auth
  loginByEmail(form: LoginI): Observable<ResponseI>{
    let address = this._url + "auth";

    // let address = this._url + "Login"; // direccion
    return this.http.post<ResponseI>(address, form);
  }

getAllUsers(page: number):Observable<ListUsersI[]>{
  // /pacientes?page=$numeroPagina
  let UrlAddress = this._url + "pacientes?page=" + page; 
  return this.http.get<ListUsersI[]>(UrlAddress);
}



}
